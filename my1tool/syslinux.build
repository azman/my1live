#!/bin/bash

# syslinux.build
# - written by azman@my1matrix.org
# - build script for syslinux (based on my1 SlackBuild scripts)

PROG_PREX=${PROG_PREX:="-pre1"}
PROG_NAME=$(basename $0 .build)
PROG_VERS="6.04" # CHANGE TO WHAT WE NEED
PROG_FULL="$PROG_NAME-$PROG_VERS$PROG_PREX"
PROG_FOLD="$PROG_FULL" # SHOULD BE TOP FOLDER'S NAME
PROG_BALL="$PROG_FULL.tar.xz"
PROG_PATH="http://www.kernel.org/pub/linux/utils/boot/$PROG_NAME"
# use pre-release version
[ ! -z "$PROG_PREX" ] && PROG_PATH="$PROG_PATH/Testing/$PROG_VERS"
PROG_ORIG="$PROG_BALL"
PROG_LOAD="$PROG_PATH/$PROG_ORIG"

# for now, using source tarball from devuan chimaera
# - enable deb-src for chimaera main repo
# - apt source syslinux (can be done as normal user)
# - current version is syslinux_6.04~git20190206.bf6db5b4+dfsg1-3
# - repacked as syslinux-6.04-git20190206.tar.xz
PROG_PREX=
PROG_NAME=$(basename $0 .build)
PROG_VERS="6.04-git20190206" # CHANGE TO WHAT WE NEED
PROG_FULL="$PROG_NAME-$PROG_VERS"
PROG_FOLD="$PROG_FULL" # SHOULD BE TOP FOLDER'S NAME
PROG_BALL="$PROG_FULL.tar.xz"
PROG_PATH="http://www.kernel.org/pub/linux/utils/boot/$PROG_NAME"
PROG_ORIG="$PROG_BALL"
PROG_LOAD="$PROG_PATH/$PROG_ORIG"

THIS_PATH=$(cd $(dirname $0);pwd)
BUILD_LIB=$THIS_PATH/libbuildtool
[ ! -f $BUILD_LIB ] &&
	echo "** Cannot find $(basename $BUILD_LIB)!" && exit 1
. $BUILD_LIB

# dir layout
BIOS_PATH=$DEST_PATH/$PROG_NAME/boot
EFI64PATH=$DEST_PATH/$PROG_NAME/EFI/Boot
# for isolinux search path
BOOT_PATH=${BOOT_PATH:="/linux/boot"}

copy_file()
{
	local file path name
	file=$1 path=$2 name=$3
	[ ! -f "$file" ] && echo "** Cannot find '$file'!" && exit 1
	[ ! -d "$path" ] && mkdir -p $path
	cp $file $path/$name
}

echo "[$PROG_NAME] Preparing source ($PROG_VERS)... "
mkdir -p $WORK_PATH
cd $WORK_PATH
rm -rf $PROG_FOLD
tar xvf $BALL_PATH/$PROG_BALL >>/dev/null 2>&1
[ ! -d "$PROG_FOLD" ] && echo "[$PROG_NAME] Source extract failed?!" && exit 1
echo "[$PROG_NAME] Source ready!"

echo -n "" >$WORK_INFO
cd $PROG_FOLD

# - only patching for static compile & isolinux.bin search path
echo "[$PROG_NAME] Patching for static compile... "
sed '/syslinux:/{n;s/$/ -static &/}' -i linux/Makefile
[ $? -ne 0 ] && echo "** error! (sys)" && exit 1
sed '/extlinux:/{n;s/$/ -static &/}' -i extlinux/Makefile
[ $? -ne 0 ] && echo "** error! (ext)" && exit 1
echo "[$PROG_NAME] Patching search paths for isolinux... "
CONF_LIST="core/fs/iso9660/iso9660.c"
CONF_LIST="$CONF_LIST core/fs/lib/loadconfig.c"
CONF_LIST="$CONF_LIST core/elflink/load_env32.c"
for file in $CONF_LIST ; do
	sed -i -e 's|"/",|"'$BOOT_PATH'",\n\t"/",|' $file
	[ $? -ne 0 ] && echo "** error! ($file)" && exit 1
done
#echo "[$PROG_NAME] Patching for macro bug... "
#sed -e 41'i #include <sys/sysmacros.h>' -i extlinux/main.c
#echo "[$PROG_NAME] Patching for ldlinux.elf bug... "
#sed -e 's/--end-group/--end-group --no-dynamic-linker/' -i core/Makefile
#echo "[$PROG_NAME] Patching for multilib option (m32) ... "
#sed -e 's|^\(CFLAGS.*\)$|CFLAGS += -m32\n\1|' -i core/Makefile
#echo "[$PROG_NAME] Patching for gpxe int13 issue... "
#sed -e 11'i CFLAGS += -fno-pie' -i gpxe/src/Makefile
echo "[$PROG_NAME] Patch completed!"

# list of files to rebuild
BIOS_LIST="bios/com32/libutil/libutil.c32"
BIOS_LIST="$BIOS_LIST bios/com32/menu/menu.c32"
BIOS_LIST="$BIOS_LIST bios/com32/menu/vesamenu.c32"
BIOS_LIST="$BIOS_LIST bios/com32/lib/libcom32.c32"
BIOS_LIST="$BIOS_LIST bios/com32/elflink/ldlinux/ldlinux.c32"
BIOS_LIST="$BIOS_LIST bios/core/isolinux.bin"
BIOS_LIST="$BIOS_LIST bios/mbr/mbr.bin"
EFI64LIST="$EFI64LIST efi64/com32/libutil/libutil.c32"
EFI64LIST="$EFI64LIST efi64/com32/menu/menu.c32"
EFI64LIST="$EFI64LIST efi64/com32/menu/vesamenu.c32"
EFI64LIST="$EFI64LIST efi64/com32/lib/libcom32.c32"
EFI64LIST="$EFI64LIST efi64/com32/elflink/ldlinux/ldlinux.e64"
echo "[$PROG_NAME] Removing files to trigger rebuild... "
for file in $BIOS_LIST EFI64LIST ; do
	rm -f $file
done
# these are removed/copied separately
rm -f bios/linux/syslinux bios/extlinux/extlinux

echo "[$PROG_NAME] Building... "
make -j 8 -i >>$WORK_INFO 2>&1
[ $? -ne 0 ] && echo "[$PROG_NAME] Build failed!" && exit 1
# temporary hack?
if [ ! -z "$PROG_PREX" ] ; then
	#make bios efi64 installer >>$WORK_INFO 2>&1
	echo "[$PROG_NAME] Building bios... "
	make bios >>$WORK_INFO 2>&1
	[ $? -ne 0 ] && echo "[$PROG_NAME] Build failed!" && exit 1
fi
echo "[$PROG_NAME] Build completed!"
# bios stuff
echo "[$PROG_NAME] Copying binaries to '$BIOS_PATH'... "
mkdir -p $BIOS_PATH
THIS_FILE="bios/linux/syslinux"
THAT_FILE=$(basename $THIS_FILE)-$PROG_VERS
copy_file $THIS_FILE $BIOS_PATH $THAT_FILE
strip -s $BIOS_PATH/$THAT_FILE
THIS_FILE="bios/extlinux/extlinux"
THAT_FILE=$(basename $THIS_FILE)-$PROG_VERS
copy_file $THIS_FILE $BIOS_PATH $THAT_FILE
strip -s $BIOS_PATH/$THAT_FILE
for file in $BIOS_LIST ; do
	copy_file $file $BIOS_PATH
done
# efi64 stuff
echo "[$PROG_NAME] Copying binaries to '$EFI64PATH'... "
THIS_FILE="efi64/efi/syslinux.efi"
THAT_FILE="BOOTX64.EFI"
copy_file $THIS_FILE $EFI64PATH $THAT_FILE
mkdir -p $EFI64PATH
for file in $EFI64LIST ; do
	copy_file $file $EFI64PATH
done
echo "[$PROG_NAME] Copy completed!"
