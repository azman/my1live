#!/bin/bash

THIS_NAME=$(basename $0 .sh)
THIS_PATH=$(cd $(dirname $0);pwd)
CONF_PATH=$(cd ${THIS_PATH}/.. ; pwd)
THIS_CONF=${CONF_PATH}/config
[ ! -f "$THIS_CONF" ] && echo "Cannot find $THIS_CONF!" && exit 1
. $THIS_CONF
THIS_BLIB=${CONF_PATH}/libbuild
[ ! -f "$THIS_BLIB" ] && echo "Cannot find $THIS_BLIB!" && exit 1
. $THIS_BLIB

TEST_HELP_TEXT=$(cat <<HELP_TEXT
Usage:
  $(basename $THIS_PATH)/$THIS_NAME [options]
Options:
  --help  | -h    : show this text
  --force | -f    : force rebuild
  --disk <path>   : path for disk root (def: $WORK_DISK)
  --path <path>   : path for ISO (def: $THIS_PATH)
  --name <live>   : name for live distro (def: $LIVE_NAME)
  --mark <label>  : name for ISO volume (def: $LIVE_MARK)
  --make          : flag to (re-)mark live boot
HELP_TEXT
)
[ "$1" = "help" ] && echo "$TEST_HELP_TEXT" && exit 0

boot_iso() {
	local that tool path
	that=$1
	tool="do-qemu"
	must_have_tool $tool
	path=$(dirname $that)
	here=$(pwd)
	cd $path
	if [ -f ".qemu" ] ; then $tool
	else echo "** Cannot find .qemu file!" ; fi
	cd $here
}

check_newitem() {
	local curr from list temp
	curr=$1
	from=$2
	list=$(find $from -type f)
	for temp in $list ; do
		[ $temp -nt $curr ] && return 1
	done
	return 0
}

build_iso() {
	local disk path name mark make qemu that tool part curr
	disk=$WORK_DISK
	path=$THIS_PATH
	name=$LIVE_NAME
	mark=$LIVE_MARK
	make=0 qemu=0
	while [ ! -z "$1" ] ; do
		case $1 in
			--disk) shift ; disk=$1 ;;
			--path) shift ; path=$1 ;;
			--name) shift ; name=$1 ;;
			--mark) shift ; mark=$1 ;;
			--force|-f) make=1 ;;
			--qemu|-q) qemu=1 ;;
			--help|-h) echo "$BOOT_HELP_TEXT" ; exit 0 ;;
			-*|*) echo "** Unknown option '$1'!" ;;
			esac
			shift
	done
	[ ! -d "$path" ] && echo "** Invalid path!" && exit 1
	path=$(cd $path;pwd)
	that=$path/${name}.iso
	part=$disk/$PART_BOOT
	if [ -f $that ] ; then
		check_newitem $that $part
		if [ $make -eq 0 -a $? -eq 0 ] ; then
			[ $qemu -eq 0 ] &&
				echo "** Image $that exists!" && exit 1
			boot_iso $that
			exit 0
		fi
		echo -n "## Removing $that ... "
		rm -rf $that
		[ $? -eq 0 ] && echo "done." || echo "error?"
	fi
	echo "-- Building ISO from $disk..."
	tool=$(which genisoimage 2>/dev/null)
	[ -z "$tool" ] && tool=$(which mkisofs 2>/dev/null)
	[ ! -x "$tool" ] && echo "** Cannot find iso tool!" && exit 1
	curr=$(pwd)
	cd $part && $tool -o $that -v -J -R -D -A "$name" -V "$mark" \
		-no-emul-boot -boot-info-table -boot-load-size 4 \
		-b linux/boot/isolinux.bin -c linux/boot/isolinux.boot .
	cd $curr
	echo "-- Image:{$that}."
	[ $qemu -ne 0 ] && boot_iso $that
}
build_iso $@
